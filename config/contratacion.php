<?php

return [
    'name' => 'Contratación Selfconta',

    'routes' => [
        'asesoria' => 'advice',
        'incluye' => 'include',
        'datos' => 'data',
        'registro' => 'register',
        'identificacion' => 'identification',
        'activacion' => 'activation',
        'fin' => 'end',
		],

	'public-path' => './',

    'info-email1' => 'joyners@selfconta.es',
    'info-email2' => 'joyners@selfconta.es',
    'info-email3' => 'borjanavarro@ayudatpymes.com',

    'phone' => '856 560 200',
    'price-alta' => '10.00',
    'price-months' => '9.99',
    'price-pay' => 19.99,

    'email' => 'juancarlosaguilera@ayudatpymes.com',
    'legal-url' => 'http://www.ayudatpymes.com/avisoLegal.html',
	'selfconta-url' => 'https://selfconta.es',
    // 'bidoq-reg-url' => 'https://dev.mispapeles.es/cargas.php',
    'bidoq-reg-url' => 'https://mispapeles.es/cargas.php',

	// test
    'momo-ISS' => 'iss-momopocket-29',
    'momo-SUB' => 'sub-AyudaTpymes',
    'momo-CLAVEPRV' => file_get_contents(resource_path('clave-privada.pem')),
    'momo-CODCOMERCIO' => 'kQMe9Drb2B',
    'momo-CODTPV' => 'LSP3iUPdrt',
    'momo-ENPRO' => false,
	
	//pro
    // 'momo-ISS' => 'iss-momopocket-9',
    // 'momo-SUB' => 'sub-AyudaTpymes',
    // 'momo-CLAVEPRV' => file_get_contents(resource_path('clave-privada-real.pem')),
    // 'momo-CODCOMERCIO' => '5g2y3x1jaQ',
    // 'momo-CODTPV' => 'aucVXRN8mX',
    // 'momo-ENPRO' => true,
];
