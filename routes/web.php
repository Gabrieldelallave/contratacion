<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('index');

Route::get('/getFile', ['uses' => 'Controller@getFile']);



Route::post('/activated', 'FrontController@activated')->name('activated');


foreach (config('contratacion.routes') as $route => $name)
{
    Route::get("/$route", "FrontController@$name")->name($name);
}
Route::post('/datos-procesar', 'FrontController@dataDo')->name('dataDo');
Route::post('/registro-procesar', 'FrontController@registerDo')->name('registerDo');
Route::post('/identificacion-procesar', 'FrontController@identificationDo')->name('identificationDo');
Route::post('/checkDocument', 'Controller@checkDocument')->name('checkDocument');

Route::get('/ko', 'FrontController@ko')->name('ko');
Route::post('/ko', 'FrontController@ko')->name('ko');
