let mix = require('laravel-mix');

mix.options({
  processCssUrls: false // Do not process/optimize relative stylesheet url()'s, for asset_()
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Commented because of compiled once
//mix.js('resources/assets/js/app.js', 'public/js')
//   .sass('resources/assets/sass/app.scss', 'public/css');

mix.sass('resources/assets/sass/front.scss', 'public/css')
  .sourceMaps();
