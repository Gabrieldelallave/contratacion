@extends('layout')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <h1 class="font-size-34px margin-bottom-lg font-weight-bold text-center">
                ¡Vas a flipar!<br>
                Alta de autónomo + Facturación + Obligaciones fiscales
            </h1>
        </div>
        <div class="col-md-3 col-md-offset-3 col-sm-12 advice-info text-center text-right-md">
            <div class="only color-celeste font-size-20px font-weight-bold text-center text-left-md">Por sólo</div>
            <span class="price font-weight-bold">
                <div class="price-1">
                    <span style="font-size:55px;">{!! explode('.',config('contratacion.price-months'))[0] !!}</span><span style="font-size:30px;">,{!! explode('.',config('contratacion.price-months'))[1] !!}€/mes</span>
                </div>
                <div class="mas">+</div>
                <div class="price-2">
                    <span class="font-size-22px">Alta {!! config('contratacion.price-alta') !!}&euro;</span>
                    <div style="margin-top: -10px;">primer mes</div>
                </div>
               
            </span>
        </div>
        <div class="col-md-4 col-sm-12 margin-top-lg text-center text-left-md" >
            <p class="color-celeste font-size-16px margin-top-lg ">
                Toda la capacidad tecnológica, <b>programas y herramientas gratis </b> por ser cliente, 
                <b>alta de autónomo, facturación y presentación de impuestos trimestrales y anuales.</b>
            </p>
            <div class="row prevnext font-size-15px  margin-top-md">
                <div class="col-sm-12">
                    <a href="{{ route('index') }}" class="btn prev margin-right-md">{{ t('app.prev') }}</a>
                    <a href="{{ route('include') }}" class="btn next">{{ t('app.next') }}</a>
                </div>
            </div>
        </div>
    </div>

    
</div>

@endsection
