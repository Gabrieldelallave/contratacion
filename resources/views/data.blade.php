@extends('layout')

@section('content')

<?php

//define dummy form fill data only for local env
function o($key)
{
    //return old($key); //normal behavior always
    $dummy = [
        'theName' => 'Prueba',
        'phone' => '123456789',
        'email' => 'Prueba@Prueba.com',
    ];
    if (old($key)) $dummy[$key] = old($key);
    return 'local' == env('APP_ENV') ? $dummy[$key] : old($key);
}

?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="font-weight-bold font-size-28px">¿Comenzamos?</h1>
            <h2 class="step color-celeste font-weight-semibold font-size-22px margin-top-xs padding-top-xs margin-bottom-lg">1. Datos</h2>

            <div class="row">

                <form class="register" role="form" method="POST" action="{{ route('dataDo') }}">
                    {{ csrf_field() }}

                    <?php //display:none by css, a trick for spam bots ?>
                    <input type="text" name="name" id="name" tabindex="-1"/>

                    <div class="col-sm-6 form-group{{ $errors->has('theName') ? ' has-error' : '' }}">
                        <label for="theName">Nombre y apellidos</label>
                        <input id="theName" type="text" class="form-control" name="theName" value="{{ o('theName') }}">
                        @if ($errors->has('theName'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('theName') }}</span>
                        @endif
                    </div>

                    <div class="col-sm-3 form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone">Teléfono</label>
                        <input id="phone" type="text" class="form-control" name="phone" value="{{ o('phone') }}">
                        @if ($errors->has('phone'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('phone') }}</span>
                        @endif
                    </div>

                    <div class="col-sm-3 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email</label>
                        <input id="email" type="text" class="form-control" name="email" value="{{ o('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('email') }}</span>
                        @endif
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-12 col-sm-12 text-center form-group{{ $errors->has('accept') ? ' has-error' : '' }}">
                        <div class="checkbox padding-top-none">
                            <label class="font-size-13px">
                                <input type="checkbox" name="accept" {{ $errors->has('accept') ? '' : 'checked' }}>
                                Acepto las <a href="{{ config('contratacion.legal-url') }}" target="_blank" class="color-celeste">condiciones del aviso legal</a>
                            </label>
                        </div>
                        @if ($errors->has('accept'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('accept') }}</span>
                        @endif
                    </div>

                    <div class="col-sm-12 col-md-12 prevnext font-size-15px text-center ">
                        <a href="{{ route('include') }}" class="btn prev margin-right-md">{{ t('app.prev') }}</a>
                        <button type="submit" class="btn next">{{ t('app.next') }}</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

<script>
$('form.register #no_ssnumber').click(function() {
    $('form.register .ssdata').addClass('hidden');
    if ($(this).prop('checked'))
    {
        $('form.register .ssdata').removeClass('hidden');
    }
});
</script>

@endsection
