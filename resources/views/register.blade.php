@extends('layout')

@section('content')

<?php

//define dummy form fill data only for local env
function o($key)
{
    //return old($key); //normal behavior always
    $dummy = [
        'idnumber' => '98872910G',
        'address' => 'Prueba',
        'postal_code' => '11100',
        'start' => '10/10/2019',
        'ssnumber' => '111065395979',
        'banknumber' => 'ES6746065541972515459926',
        'father_name' => 'Prueba',
        'mother_name' => 'Prueba',
        'city_birth' => 'Prueba',
        'country_birth' => 'Prueba',
        'coverage' => 'acogerse',
    ];
    if (old($key)) $dummy[$key] = old($key);
    return 'local' == env('APP_ENV') ? $dummy[$key] : old($key);
}

?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="font-weight-bold font-size-28px"><br></h1>
            <h2 class="step color-celeste font-weight-semibold font-size-22px margin-top-xs padding-top-xs margin-bottom-lg">2. Registro</h2>

    
            <div class="row">

                <form class="register" role="form" method="POST" action="{{ route('registerDo') }}">
                    {{ csrf_field() }}

                    <?php //display:none by css, a trick for spam bots ?>
                    <input type="text" name="name" id="name" tabindex="-1"/>

                    <?php
                    $data = session('register_data');
                    foreach (['theName', 'phone', 'email'] as $foo)
                    {
                        echo "<input type='hidden' name='$foo' value='{$data[$foo]}'/>";
                    }
                    ?>

                    <div class="col-sm-3 form-group{{ $errors->has('idnumber') ? ' has-error' : '' }}">
                        <label for="idnumber">DNI / NIE</label>
                        <input id="idnumber" type="text" class="form-control" name="idnumber" value="{{ o('idnumber') }}">
                        @if ($errors->has('idnumber'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('idnumber') }}</span>
                        @endif
                    </div>

                    <div class="col-sm-6 form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <label for="address">Dirección</label>
                        <input id="address" type="text" class="form-control" name="address" value="{{ o('address') }}">
                        @if ($errors->has('address'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('address') }}</span>
                        @endif
                    </div>

                    <div class="col-sm-3 form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                        <label for="postal_code">Código postal</label>
                        <input id="postal_code" type="text" class="form-control" name="postal_code" value="{{ o('postal_code') }}">
                        @if ($errors->has('postal_code'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('postal_code') }}</span>
                        @endif
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-3 form-group{{ $errors->has('start') ? ' has-error' : '' }}">
                        <label for="start">¿Cuándo inicias el alta?</label>
                        <input id="start" type="text" class="form-control" name="start" value="{{ o('start') }}">
                        @if ($errors->has('start'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('start') }}</span>
                        @endif
                    </div>

                    <div class="col-sm-3 form-group{{ $errors->has('ssnumber') ? ' has-error' : '' }}">
                        <label for="ssnumber">Número seg. social</label>
                        <input id="ssnumber" type="text" class="form-control" name="ssnumber" value="{{ o('ssnumber') }}">
                        @if ($errors->has('ssnumber'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('ssnumber') }}</span>
                        @endif
                    </div>

                    <div class="col-sm-6 form-group{{ $errors->has('banknumber') ? ' has-error' : '' }}">
                        <label for="banknumber">Número de cuenta (IBAN)</label>
                        <input id="banknumber" type="text" class="form-control" name="banknumber" value="{{ o('banknumber') }}">
                        @if ($errors->has('banknumber'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('banknumber') }}</span>
                        @endif
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-xs-12 form-group{{ $errors->has('coverage') ? ' has-error' : '' }}">
                        Cobertura de las contingencias profesionales
                        <span class="coverage-info font-weight-bold" data-toggle="modal" data-target="#coverage-info-modal">i</span>
                        <div class="clearfix"></div>
                        <label class="radio-inline">
                            <input type="radio" name="coverage" id="cov1" value="acogerse"{{ 'acogerse' == o('coverage') ? ' checked' : '' }}>Acogerse
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="coverage" id="cov2" value="renunciar"{{ 'renunciar' == o('coverage') ? ' checked' : '' }}>Renunciar
                        </label>
                        @if ($errors->has('coverage'))
                            <span class="help-block margin-bottom-none">{{ $errors->first('coverage') }}</span>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal fade" id="coverage-info-modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Cobertura contingencias profesionales</h4>
                                </div>
                                <div class="modal-body">
                                    Si deseas estar cubierto en caso de accidente de trabajo o enfermedad profesional debes pulsar "Acogerse".
                                    Desde Selfconta, te aconsejamos acogerte a esta cobertura ya que el riesgo de accidente laboral es considerable por el tipo de actividad que ejercen, ya que estáis en continuo movimiento.
                                    Acogerse conlleva entre 20-30 euros más de coste en tu cuota de autónomo.
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 form-group">
                        <div class="checkbox padding-top-none">
                            <label class="font-size-13px">
                                <input type="checkbox" id="no_ssnumber" name="no_ssnumber"{{ old('no_ssnumber') ? ' checked' : '' }}>
                                No tengo número de seguridad social
                            </label>
                        </div>

                        <div class="row ssdata{{ old('no_ssnumber') ? '' : ' hidden' }}">
                            <div class="col-md-6 form-group{{ $errors->has('father_name') ? ' has-error' : '' }}">
                                <label for="father_name">Nombre y apellidos de tu padre</label>
                                <input id="father_name" type="text" class="form-control" name="father_name" value="{{ o('father_name') }}">
                                @if ($errors->has('father_name'))
                                    <span class="help-block margin-bottom-none">{{ $errors->first('father_name') }}</span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group{{ $errors->has('mother_name') ? ' has-error' : '' }}">
                                <label for="mother_name">Nombre y apellidos de tu madre</label>
                                <input id="mother_name" type="text" class="form-control" name="mother_name" value="{{ o('mother_name') }}">
                                @if ($errors->has('mother_name'))
                                    <span class="help-block margin-bottom-none">{{ $errors->first('mother_name') }}</span>
                                @endif
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 form-group{{ $errors->has('city_birth') ? ' has-error' : '' }}">
                                <label for="city_birth">Ciudad de nacimiento</label>
                                <input id="city_birth" type="text" class="form-control" name="city_birth" value="{{ o('city_birth') }}">
                                @if ($errors->has('city_birth'))
                                    <span class="help-block margin-bottom-none">{{ $errors->first('city_birth') }}</span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group{{ $errors->has('country_birth') ? ' has-error' : '' }}">
                                <label for="country_birth">País de nacimiento</label>
                                <input id="country_birth" type="text" class="form-control" name="country_birth" value="{{ o('country_birth') }}">
                                @if ($errors->has('country_birth'))
                                    <span class="help-block margin-bottom-none">{{ $errors->first('country_birth') }}</span>
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 prevnext font-size-15px text-center">
                       
                        <a href="{{ route('data') }}" class="btn prev margin-right-md">{{ t('app.prev') }}</a>
                        <button type="submit" class="btn next">{{ t('app.next') }}</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

<script>
$('form.register #no_ssnumber').click(function() {
    $('form.register .ssdata').addClass('hidden');
    if ($(this).prop('checked'))
    {
        $('form.register .ssdata').removeClass('hidden');
    }
});
</script>

@endsection
