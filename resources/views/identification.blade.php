@extends('layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="step color-celeste font-weight-semibold font-size-22px margin-top-xs padding-top-xs margin-bottom-lg">3. Identificación</h2>

            <form role="form" method="POST" action="{{ route('identificationDo') }}">
                {{ csrf_field() }}

                <p>Sube aquí tu DNI/NIE (imagen tipo gif, jpg o png), recuerda que debe contener las dos caras, delantera y trasera. Si lo tienes por separado adjunta dos imágenes.</p>
                <div id="iddocs">
                    <div class="row margin-bottom-sm">
                        <div class="col-xs-6 col-sm-4">
                            <img id="iddoc1" width="100%" class="hidden" src="{{ asset_('images/1px.png') }}"/>
                            <input type="hidden" id="iddoc1file" name="iddoc1file">
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <img id="iddoc2" width="100%" class="hidden" src="{{ asset_('images/1px.png') }}"/>
                            <input type="hidden" id="iddoc2file" name="iddoc2file">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="progress-box hidden row margin-bottom-sm">
                        <div class="col-xs-6 col-sm-4">
                            <div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped"></div></div>
                        </div>
                    </div>
                    <div class="form-group select">
                        <span class="btn btn-default fileinput-button">
                            <span>Seleccionar imagen(es) DNI/NIE</span>
                            <input id="fileupload" type="file" name="file" accept="image/gif,image/jpeg,image/png">
                        </span>
                        <span class="help-block margin-bottom-none font-size-13px"></span>
                    </div>
                </div>

                <?php $sess = session('register_data'); if (isset($sess['no_ssnumber'])) : ?>
                <p class="margin-top-40px">Aquí puedes subir una imagen de tu pasaporte.</p>
                <div id="passport">
                    <div class="row margin-bottom-sm">
                        <div class="col-xs-6 col-sm-4">
                            <img id="iddoc3" width="100%" class="hidden" src="{{ asset_('images/1px.png') }}"/>
                            <input type="hidden" id="iddoc3file" name="iddoc3file">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="progress-box hidden row margin-bottom-sm">
                        <div class="col-xs-6 col-sm-4">
                            <div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped"></div></div>
                        </div>
                    </div>
                    <div class="form-group select">
                        <span class="btn btn-default fileinput-button">
                            <span>Seleccionar imagen pasaporte</span>
                            <input id="fileupload" type="file" name="file" accept="image/gif,image/jpeg,image/png">
                        </span>
                        <span class="help-block margin-bottom-none font-size-13px"></span>
                    </div>
                </div>
                <?php endif; ?>

            </form>
        </div>
    </div>

    <div class="row prevnext font-size-15px text-center">
        <div class="col-xs-12">
            <a href="{{ route('register') }}" class="btn prev margin-right-md">{{ t('app.prev') }}</a>
            <a href="javascript:void(0)" class="btn next" disabled>{{ t('app.next') }}</a>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="{{ asset('js/jQuery-File-Upload/css/jquery.fileupload.css') }}">
<script src="{{ asset('js/jQuery-File-Upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/jQuery-File-Upload/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('js/jQuery-File-Upload/js/jquery.fileupload.js') }}"></script>
<script>

//enables the "next" button neccesary images are uploaded
function atg_enable_next()
{
    var enable = true;
    if ('<?=asset_('images/1px.png')?>' == $('#iddocs img#iddoc1').attr('src')) enable = false;
    if ($('#passport').length && '<?=asset_('images/1px.png')?>' == $('#passport img#iddoc3').attr('src')) enable = false;
    if (enable) $('.btn.next').attr('disabled', null);
}

$('.btn.next').click(function() {
    if (!$(this).attr('disabled'))
    {
        $('form').submit();
    }
});

$('#iddocs #fileupload').fileupload({
    url: '<?=route('checkDocument')?>',
    type: 'post',
    dataType: 'json',
    start: function (e, data) {
        $('#iddocs .select .btn').hide();
        $('#iddocs .progress-box').removeClass('hidden');
    },
    done: function (e, data) {
        if (data.result.error)
        {
            $('#iddocs .select').addClass('has-error');
            $('#iddocs .select .help-block').html(data.result.error);
        }
        else if (data.result.url)
        {
            //the two positions filled, remove and start again
            if ('<?=asset_('images/1px.png')?>' != $('#iddocs img#iddoc1').attr('src') && '<?=asset_('images/1px.png')?>' != $('#iddocs img#iddoc2').attr('src'))
            {
                $('#iddocs img#iddoc1, #iddocs img#iddoc2').addClass('hidden').attr('src', '<?=asset_('images/1px.png')?>');
                $('#iddocs #iddoc1file, #iddocs #iddoc2file').val('');
            }

            //paint file viewing in pos 1st or if filled, then 2nd
            if ('<?=asset_('images/1px.png')?>' == $('#iddocs img#iddoc1').attr('src'))
            {
                $('#iddocs img#iddoc1').attr('src', data.result.url).removeClass('hidden');
                $('#iddocs #iddoc1file').val(data.result.tmp);
            }
            else if ('<?=asset_('images/1px.png')?>' == $('#iddocs img#iddoc2').attr('src'))
            {
                $('#iddocs img#iddoc2').attr('src', data.result.url).removeClass('hidden');
                $('#iddocs #iddoc2file').val(data.result.tmp);
            }

            atg_enable_next();
        }
        setTimeout("$('#iddocs .select .btn').show(); $('#iddocs .progress-box').addClass('hidden').find('.progress-bar').css('width',0).html('');", 555);
    },
    progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#iddocs .progress .progress-bar').css('width', progress+'%').html(progress+'%');
    }
});

$('#passport #fileupload').fileupload({
    url: '<?=route('checkDocument')?>',
    type: 'post',
    dataType: 'json',
    start: function (e, data) {
        $('#passport .select .btn').hide();
        $('#passport .progress-box').removeClass('hidden');
    },
    done: function (e, data) {
        if (data.result.error)
        {
            $('#passport .select').addClass('has-error');
            $('#passport .select .help-block').html(data.result.error);
        }
        else if (data.result.url)
        {
            //if filled, remove and start again
            if ('<?=asset_('images/1px.png')?>' != $('#passport img#iddoc3').attr('src'))
            {
                $('#passport img#iddoc3').addClass('hidden').attr('src', '<?=asset_('images/1px.png')?>');
                $('#passport #iddoc3file').val('');
            }

            //paint file viewing
            $('#passport img#iddoc3').attr('src', data.result.url).removeClass('hidden');
            $('#passport #iddoc3file').val(data.result.tmp);

            atg_enable_next();
        }
        setTimeout("$('#passport .select .btn').show(); $('#passport .progress-box').addClass('hidden').find('.progress-bar').css('width',0).html('');", 555);
    },
    progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#passport .progress .progress-bar').css('width', progress+'%').html(progress+'%');
    }
});
</script>

@endsection
