@extends('layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h1 class="font-weight-bold font-size-60px">Upsss!</h1>
            <div class="margin-top-lg font-weight-bold color-celeste upper">Algo no ha ido bien</div>
            <div class="margin-top-md color-celeste">Por favor inténtalo de nuevo</div>
            <a href="{{ route('register') }}" class="btn btn-orange margin-top-30px">Vale</a>
        </div>
    </div>
</div>

@endsection
