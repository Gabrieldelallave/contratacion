@extends('layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h1 class="font-weight-bold font-size-60px">¡Enhorabuena!</h1>
            <div class="font-weight-bold margin-top-lg color-celeste upper">
                TU ALTA DE AUTÓNOMO YA SE ESTÁ TRAMITANDO
            </div>
            <div class="font-weight-bold color-celeste upper">
                Nos pondremos en contacto contigo a la mayor brevedad.
            </div>
            <div class="margin-top-md color-celeste">Muchas gracias por confiar en nosotros.</div>
            <a href="{{ url('/') }}" class="btn btn-orange margin-top-30px">FINALIZAR</a>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $.post("<?=config('contratacion.bidoq-reg-url')?>", {
            'form_ajax':'nuevoCliente',
            'username': '<?=$data['theName']?>',
            'contacto': '',
            'email': '<?=$data['email']?>',
            'dni': '<?=$data['idnumber']?>',
            'cif': '',
            'telefono': '<?=$data['phone']?>',
            'direccion': '<?=$data['address']?>',
            'actividad': '26', //otros
            'provincias': '12', //Cádiz
            'comienzoActividad': '<?=date('Y', time())  //año actual ?>',
            'num_empleados': '1', //1
            'cuentaAyudaT': '<?=$data['banknumber']?>',
            'forma': 'autonomo',
            'tipo': 'contratacion',
            'cuota': '9.95'
        });
    });
</script>

@endsection
