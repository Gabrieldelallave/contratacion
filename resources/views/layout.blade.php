<?php $routeName = Illuminate\Support\Facades\Route::current()->getName(); ?>
<?php //<!DOCTYPE html> ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Selfconta Contratación</title>

    <link rel="icon" type="image/png" href="{{ asset_('images/favicons/16.png') }}" sizes="16x16">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    

    <link href="{{ asset_('css/front.css') }}" rel="stylesheet">

</head>
<body class="{{ $routeName }}" data-img1px="{{ asset_('images/1px.png') }}">
    <?php if ('local' == env('APP_ENV')) var_dump(session('register_data')) ?>
    <div class="container theHeader">
        <div class="row">
            <div class="l col-sm-6 text-center text-left-sm">
                <a href="{{ url('https://selfconta.es') }}"  target="_blank" class="logo-selfconta"></a>
            </div>
            <div class="r col-sm-6 text-center text-right-sm">
                <span class="font-size-14px font-weight-semibold">{{ t('app.info-help') }}</span>
                <a href="tel:{{ config('contratacion.phone') }}" class="font-size-20px font-weight-bold">{{ t('app.t-phone', 1, ['phone' => config('contratacion.phone')]) }}</a>
            </div>
        </div>
    </div>

    @yield('content')

    <div class="container margin-top-80px theFooter padding-bottom-40px">
        <div class="row">
            <div class="l col-sm-6">
                <a href="{{ url('https://selfconta.es') }}"  target="_blank" class="logo-selfconta-gray"></a>
            </div>
            <div class="r col-sm-6 font-size-10px text-center text-right-sm">
                <div class="ayudatgroup">
                    <span class=""></span><br>
                    {{ t('app.selfconta-legal') }} | <a href="{!! config('contratacion.legal-url') !!}">{{ t('app.legal-advice') }}</a>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset_('js/app.js') }}"></script>
    <script src="{{ asset_('js/front.js') }}"></script>

</body>
</html>
