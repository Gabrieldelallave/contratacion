@extends('layout')

@section('content')

<div class="container">
        <div class="row text-center">
            <div class="col-sm-4 col-md-5">
                <span class="devices" style="float:right;"></span>
            </div>
            <div class="col-sm-8 col-md-6">
                <h1 class="font-size-28px font-weight-bold text-center text-left-sm color-verde">¿Qué incluimos por {!! config('contratacion.price-months') !!}€ / mes?</h1>
                <ul class="font-size-18px color-celeste text-center text-left-sm">
                    <li><b>Alta autónomo</b> o cambio de asesoría</li>
                    <li><b>Pricipales obligaciones fiscales</b> que tienes como autónomo</li>
                    <li><b>Llamada inicial para gestionar tu actividad</b></li>
                    <li><b>Presentación de impuestos</b></li>
                    <li>Selfconta, <b>programa gratuito de facturación y contabilidad</b></li>
                </ul>
            </div>
            <div class="col-sm-8 col-md-6 prevnext font-size-15px text-center text-left-sm">
                <a href="{{ route('advice') }}" class="btn prev margin-right-md">{{ t('app.prev') }}</a>
                <a href="{{ route('data') }}" class="btn next">{{ t('app.next') }}</a><br>
            </div>
        </div>

</div>

@endsection
