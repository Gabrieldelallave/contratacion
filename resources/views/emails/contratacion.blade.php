@component('mail::message')
# OK Contratación

Datos de la contratación:

@include('emails.data', ['data' => $data])

Datos del pago:

<?php foreach (['codExternalOp', 'returnCode'] as $key) : ?>
{{ t('attr.' . $key) }}: {{ $data[$key] }}<br>
<?php endforeach; ?>

@endcomponent
