<?php foreach ([
    'theName',
    'idnumber',
    'address',
    'postal_code',
    'phone',
    'email',
    'start',
    'ssnumber',
    'banknumber',
    'coverage',
    ] as $key) : ?>
{{ t('attr.' . $key) }}: {{ $data[$key] }}<br>
<?php endforeach; ?>

@if (isset($data['no_ssnumber']) && $data['no_ssnumber'])

    Sin número de la seguridad social, por lo tanto:

    <?php foreach ([
        'father_name',
        'mother_name',
        'city_birth',
        'country_birth'
        ] as $key) : ?>
    {{ t('attr.' . $key) }}: {{ $data[$key] }}<br>
    <?php endforeach; ?>

@endif
