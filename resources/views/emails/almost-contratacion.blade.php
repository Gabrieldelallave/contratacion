@component('mail::message')
# CASI Contratación

Datos de la posible contratación:

@include('emails.data', ['data' => $data])

@endcomponent
