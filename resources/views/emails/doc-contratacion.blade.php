@component('mail::message')
# CASI Contratación con documentación

Datos de la posible contratación:

@include('emails.data', ['data' => $data])

@endcomponent
