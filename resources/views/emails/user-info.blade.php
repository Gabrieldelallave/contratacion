@component('mail::message')
# Alta en Selfconta

Hola, bienvenid@ a Selfconta!

En primer lugar queremos agradecer la confianza que has depositado en nuestra firma. 

Recuerda que este servicio no dispone de asesor pero el próximo día hábil nos pondremos en contacto contigo de forma puntual para tramitar tu alta y solucionar las dudas que tengas en relación a la gestión.

Ahora necesitamos  que descargues los 2 documentos adjuntos en este email, los completes con la información solicitada y nos los devuelvas en respuesta a este correo. Si tienes cualquier duda en relación a los documentos adjuntos, no te preocupes, mañana te lo solucionará tu gestor.

Gracias y un saludo!

@endcomponent
