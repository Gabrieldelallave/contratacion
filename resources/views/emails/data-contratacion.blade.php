@component('mail::message')
# DATOS Contratación

Datos de la posible contratación:

<?php foreach ([
    'theName',
    'phone',
    'email',
    ] as $key) : ?>
{{ t('attr.' . $key) }}: {{ $data[$key] }}<br>
<?php endforeach; ?>

@endcomponent
