@extends('layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="step color-celeste font-weight-semibold font-size-22px margin-top-xs padding-top-xs margin-bottom-xs">4. Activar el servicio</h2>
            <div class="row margin-bottom-lg">
                <div class="col-sm-12 col-md-4 text-center text-left-md font-weight-semibold" style="line-height: 1.2;">
                    Ya casi estamos, sólo queda hacer el pago inicial.*<br>
                    No olvides devolvernos completados los documentos que te enviaremos al email facilitado.
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 font-weight-semibold text-center">
                    Tipos de pagos aceptados
                </div>
                <div class="col-sm-12 col-md-12 font-weight-semibold text-center">
                    <div class="pagos"></div>

                </div>
                <div class="col-sm-12 col-md-12 font-weight-semibold text-center margin-bottom-md">
                    Si tiene alguna duda sobre el importe o la pasarela de pago:
                    <br>
                    <a href="tel:{{ config('contratacion.phone') }}" class="step color-celeste font-size-20px font-weight-bold">{{ t('app.t-phone', 1, ['phone' => config('contratacion.phone')]) }}</a>
                </div>
                <div class="col-sm-12 col-md-12 font-size-13px font-weight-semibold margin-bottom-md">
                    <div class="bg-gray text-center">* El primer pago son {{ config('contratacion.price-pay') }}€ y los siguientes {{ config('contratacion.price-months') }}€ al mes serán domiciliados en tu cuenta bancaria.</div>
                </div>
                <div class="col-sm-12 col-md-12 text-center margin-bottom-md">
                        <a href="{{ route('activation') }}?go=1" class="btn btn-orange font-weight-semibold">{{ t('app.pay-secure') }}</a>
                    </div>
            </div>

        </div>
    </div>
</div>

@endsection
