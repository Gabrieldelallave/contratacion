@extends('layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 text-center" style="padding-left:25px;padding-right: 25px;">
            <h1 class="font-size-60px font-weight-bold margin-bottom-lg">¡Hola!<br><span class="font-size-34px">Estás de suerte :)</span></h1>
            <p class="color-celeste">
                <b>Somos Selfconta</b>, un software de facturación y contabilidad online que lucha por
                <b>facilitar la gestión de las Pymes y Autónomos en España</b>, y te damos la bienvenida a nuestra plataforma.
            </p>
            <p class="color-celeste">
                Regístrate y obtendrás incuido el <b>alta de autónomo, facturación y presentación de impuestos trimestrales y anuales.</b>
            </p>
        </div>
    </div>

    <div class="row prevnext font-size-15px text-center">
        <div class="col-xs-12">
            <a href="{{ route('advice') }}" class="btn next">{{ t('app.next') }}</a>
        </div>
    </div>
</div>

@endsection
