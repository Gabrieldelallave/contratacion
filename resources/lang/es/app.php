<?php

return [
    'info-help' => 'Información y consulta:',
    't-phone' => 'T. :phone',
    'ayudat-place' => 'Ayuda-T un lugar todas las Soluciones S.L.',
    'selfconta-legal' => 'SELFCONTA &copy; '.date('Y', time()),
    'legal-advice' => 'Aviso legal',
    'prev' => 'anterior',
    'next' => 'siguiente',
    'pay-secure' => 'Pasarela de pago segura',

    'data-contratacion.subject' => 'Datos Contratación',
    'almost-contratacion.subject' => 'Casi Contratación',
    'doc-contratacion.subject' => 'Casi Contratación documentación',
    'contratacion.subject' => 'OK Contratación',
    'user-info.subject' => 'Alta en Selfconta',
    'altaCliente.subject' => 'Alta en Selfconta',
];
