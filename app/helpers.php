<?php

use App\Text;
use Illuminate\Support\Facades\Auth;

/**
 * Shortener for "trans_choice", including use of table "texts"
 *
 * @param  string $key Key of the string to be tranlated.
 * @param  integer $amount Number of elements to determine singular or plural.
 * @param  array $replace [Key => Value] pairs to be replaced in the i18n string.
 * @param  bool $editable put this to false avoid to add the editable span tab wrapper
 * @return \Illuminate\Http\Response
 */
function t($key, $amount = 1, $replace = [])
{
    return trans_choice($key, $amount, $replace);
}

function asset_($asset)
{
    if (config('contratacion.public-path'))
    {
        $path = config('contratacion.public-path');
    } else {
        $path = public_path();
    }
    return asset($asset) .'?_=' . filemtime($path.'/'.$asset);
}

function idnumber($idnumber, $type)
{
    $ok = true;

    if ($type == 'dni' || $type == 'nif')
    {
        //replace ES prefix if exists
        $idnumber = str_replace('ES', '', $idnumber);

        $letter = substr($idnumber, -1);
        $numbers = substr($idnumber, 0, -1);
        try {
            if (substr("TRWAGMYFPDXBNJZSQVHLCKE", $numbers % 23, 1) != $letter || 8 != strlen($numbers))
                $ok = false;
        } catch(Exception $e) {
            $ok = false;
        }
    }

    if ($type == 'cif')
    {
        if (
            preg_match('/^[ABEH][0-9]{8}$/i', $idnumber) ||
            preg_match('/^[KPQS][0-9]{7}[A-J]$/i', $idnumber) ||
            preg_match('/^[CDFGJLMNRUVW][0-9]{7}[0-9A-J]$/i', $idnumber)
        ) {
            $control = $idnumber[strlen($idnumber) - 1];
            $sumA = 0;
            $sumB = 0;

            for ($i = 1; $i < 8; $i++)
            {
                if ($i % 2 == 0)
                    $sumA += intval($idnumber[$i]);
                else
                {
                    $t = (intval($idnumber[$i]) * 2);
                    $p = 0;

                    for ($j = 0; $j < strlen($t); $j++) $p += substr($t, $j, 1);
                    $sumB += $p;
                }
            }

            $sumC = (intval($sumA + $sumB)) . '';
            $sumD = (10 - intval($sumC[strlen($sumC) - 1])) % 10;

            $letters = 'JABCDEFGHI';

            if ($control >= '0' && $control <= '9') $ok = ($control == $sumD);
            else $ok = (strtoupper($control) == $letters[$sumD]);
        }
        else $ok = false;
    }

    if ($type == 'nie')
    {
        $nif_regex = '/^[0-9]{8}[A-Z]$/i';
        $nie_regex = '/^[XYZ][0-9]{7}[A-Z]$/i';

        $letters = 'TRWAGMYFPDXBNJZSQVHLCKE';

        if (preg_match($nif_regex, $idnumber))
            $ok = ($letters[(substr($idnumber, 0, 8) % 23)] == $idnumber[8]);
        else if (preg_match($nie_regex, $idnumber))
        {
          if      ($idnumber[0] == 'X') $idnumber[0] = '0';
          else if ($idnumber[0] == 'Y') $idnumber[0] = '1';
          else if ($idnumber[0] == 'Z') $idnumber[0] = '2';

          $ok = ($letters[(substr($idnumber, 0, 8) % 23)] == $idnumber[8]);
        }
        else $ok = false;
    }

    return $ok;
}

function is_idnumber($idnumber, $types = ['dni', 'nie'])
{
    $ret = false;
    foreach ($types as $type)
    {
        if (idnumber(strtoupper($idnumber), $type)) $ret = true;
    }
    return $ret;
}

function is_nass($nss) {
    if (strlen($nss) == 12)
    {
        $na = substr($nss, 0, 2);
        $nb = substr($nss, 2, 8);
        $nc = substr($nss, 10, 2);
    } else return false;

    if ($nb < 10000000)
    {
        $nd = $nb + $na * 10000000;
    } else {
        $nd = $na . $nb;
    }

    if ($nd % 97 == $nc) return true;
}

function toAscii($str, $replace = array(), $delimiter = '-') {
    setlocale(LC_CTYPE,'es_ES.utf8');

    if (!empty($replace))
    $str = str_replace((array)$replace, ' ', $str);

    //detecta si utf8
    $encoding = mb_detect_encoding($str, mb_detect_order(), false);
    if ($encoding == "UTF-8") $str = mb_convert_encoding($str, 'UTF-8', 'UTF-8');
    $str = iconv(mb_detect_encoding($str, mb_detect_order(), false), "UTF-8//IGNORE", $str);

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}
