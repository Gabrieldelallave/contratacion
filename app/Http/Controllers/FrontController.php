<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use Illuminate\Support\Facades\Mail;

use App\Mail\DataContratacion;
use App\Mail\AlmostContratacion;
use App\Mail\DocContratacion;
use App\Mail\Contratacion;
use App\Mail\UserInfo;

use App\MomoAPICli;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Formatter\LineFormatter;
use Illuminate\Support\Facades\Redirect;
use App\Mail\AltaAutonomo;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function activation(Request $request)
    {
        if (!session('register_data')) return redirect('/');

        if (1 == $request['go'])
        {
            $momo = new MomoAPICli(
                config('contratacion.momo-ISS'),
                config('contratacion.momo-SUB'),
                config('contratacion.momo-CLAVEPRV'),
                config('contratacion.momo-CODCOMERCIO'),
                config('contratacion.momo-CODTPV'),
                config('contratacion.momo-ENPRO')
            );

            $idPayment = time().'_'.uniqid();

            session(['payment-dt' => date('c', time())]);

            $data = session('register_data');

            $concept = 'contratacion: ' . $data['idnumber'] .' - '. $data['theName'] .' - '. $data['start'];
            
            return Redirect::to($momo->inicioPagoWeb(
                config('contratacion.price-pay'),
                session('payment-dt'),
                $idPayment,
                route('activated'),
                route('ko'),
                $concept,
                $data['email'],
                null,
                [ ['concepto' => $concept, 'importe' => config('contratacion.price-pay')] ]
            ));
            
        }
        else return view('activation');
    }

    public function activated(Request $request)
    {  
        if (!session('register_data')) return redirect('/');
        
        $momo = new MomoAPICli(
            config('contratacion.momo-ISS'),
            config('contratacion.momo-SUB'),
            config('contratacion.momo-CLAVEPRV'),
            config('contratacion.momo-CODCOMERCIO'),
            config('contratacion.momo-CODTPV'),
            config('contratacion.momo-ENPRO')
        );

        $checkData = ['codExternalOp' => 'Ninguno', 'returnCode' => 'Ninguno'];

        try {
          $datos = $momo->comprobarPagoWeb(session('payment-dt'), $request['uuidRetorno']);
          $checkData = ['codExternalOp' => $datos['codOperacionExterno'], 'returnCode' => $datos['codigoRetorno']];
        } catch (Exception $e) {}

        //abort on error
        if ('0502' != $checkData['returnCode']) header('Location: '.route('ko'));

        $returnCodes = [
          '0501' => 'La operación no existe',
          '0502' => 'Pago realizado con éxito',
          '0503' => 'Operación finalizada pero con algún error',
          '0504' => 'Operación no finalizada',
        ];

        $data = session('register_data');
        $data['codExternalOp'] = $checkData['codExternalOp'];
        $data['returnCode'] = $checkData['returnCode'] .' '. $returnCodes[$checkData['returnCode']];
        session(['register_data' => $data]);

        //logResult
        $concept = 'contratacion: ' . $data['idnumber'] .' - '. $data['theName'] .' - '. $data['start'];

        $logValores = 
            $data['returnCode'].
            ' | Nombre: '.$data['theName'].
            ' | DNI/CIF: '.$data['idnumber'].
            ' | Concepto: '.$concept.
            ' | Importe: '.config('contratacion.price-pay').
            ' | iban: '.$data['banknumber'].
            ' | CodOpExterno: '.$data['codExternalOp'];

        $log = new Logger('contratacion');
        $formatter = new LineFormatter(null, null, false, true); 
        $handler = new StreamHandler(storage_path().'/logs/'.'contratacion'.'.log', Logger::INFO);
        $handler->setFormatter($formatter);
        $log->pushHandler($handler);
        $log->pushHandler(new FirePHPHandler());
        $log->info($logValores);


        Mail::send(new Contratacion());
        if (count(Mail::failures()) > 1) return redirect(route('ko'));

        Mail::send(new UserInfo());
        if (count(Mail::failures()) > 1) return redirect(route('ko'));

        //mandamos el alta al cliente
        // Mail::send(new AltaCliente());
        // if (count(Mail::failures()) > 1) return redirect(route('ko'));

        session(['end_ok' => true]);
        return redirect(route('end'));
    }

    public function identificationDo(Request $request)
    {
        $data = session('register_data');
        $data['iddoc1file'] = $request['iddoc1file'];
        $data['iddoc2file'] = $request['iddoc2file'];
        $data['iddoc3file'] = $request['iddoc3file'];
        session(['register_data' => $data]);

        Mail::send(new DocContratacion());
        if (count(Mail::failures()) > 1) return redirect(route('ko'));

        return redirect(route('activation'));
    }

    public function registerDo(Request $request)
    {
        if (isset($request['name']) && $request['name'] != '') abort(418); //i'm a teapot! (trick to avoid spamming)

        foreach ([' ','.','-','+','/'] as $foochar)
        {
            foreach (['ssnumber', 'banknumber'] as $param)
            {
                $request[$param] = str_replace($foochar, '', $request[$param]);
            }
        }

        $validations = [
            'idnumber' => 'required',
            'address' => 'required',
            'postal_code' => 'required|digits:5',
            'start' => 'required|date_format:d/m/Y|after:tomorrow',
            'ssnumber' => 'digits:12',
            'banknumber' => 'required|size:24|regex:/^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}/',
            'coverage' => 'required',
        ];

        if (isset($request['no_ssnumber']))
        {
            //$request['ssnumber'] = '';
            $validations['father_name'] = 'required';
            $validations['mother_name'] = 'required';
            $validations['city_birth'] = 'required';
            $validations['country_birth'] = 'required';
        }

        $validator = Validator::make($request->all(), $validations);

        $validator->after(function($validator) use ($request)
        {
            $request['idnumber'] = strtoupper($request['idnumber']);
            if (!is_idnumber($request['idnumber']))
            {
                $validator->errors()->add('idnumber', 'El número de DNI / NIE es incorrecto.');
            }

            $request['ssnumber'] = str_replace(' ', '', $request['ssnumber']);
            $request['ssnumber'] = preg_replace('[^0-9]', '', $request['ssnumber']);
            if ($request['ssnumber'] && !is_nass($request['ssnumber']))
            {
                $validator->errors()->add('ssnumber', 'El número de seg. social es incorrecto.');
            }
        });

        if ($validator->fails())
        {
            return redirect(route('register'))
                ->withErrors($validator)
                ->withInput();
        }

        $data1 = $request->all();
        unset($data1['_token']);
        $data2 = session('register_data');
        session(['register_data' => array_merge($data1, $data2)]);

        Mail::send(new AlmostContratacion($request));
        if (count(Mail::failures()) > 1) return redirect(route('ko'));

        return redirect(route('identification'));
    }

    public function dataDo(Request $request)
    {
        if (isset($request['name']) && $request['name'] != '') abort(418); //i'm a teapot! (trick to avoid spamming)

        foreach ([' ','.','-','+','/'] as $foochar)
        {
            $request['phone'] = str_replace($foochar, '', $request['phone']);
        }

        $validations = [
            'theName' => 'required',
            'phone' => 'required|digits_between:6,9',
            'email' => 'required|email',
            'accept' => 'required',
        ];

        $validator = Validator::make($request->all(), $validations);

        if ($validator->fails())
        {
            return redirect(route('data'))
                ->withErrors($validator)
                ->withInput();
        }

        $data = $request->all();
        unset($data['_token']);
        session(['register_data' => $data]);

        Mail::send(new DataContratacion($request));
        if (count(Mail::failures()) > 1) return redirect(route('ko'));

        return redirect(route('register'));
    }



    public function index() {
        session(['register_data' => null]);
        return view('index');
    }

    public function advice() {
        session(['register_data' => null]);
        return view('advice');
    }

    public function include() {
        session(['register_data' => null]);
        return view('include');
    }

    public function data() {
        session(['register_data' => null]);
        return view('data');
    }

    public function register() {
        if (!session('register_data')) return redirect('/');
        return view('register');
    }

    public function identification() {
        
        if (!session('register_data')) return redirect('/');
        return view('identification');
    }

    public function end() {
        if (!session('end_ok')) return redirect('/');
        $data = session('register_data');

        session(['register_data' => null]);
        session(['end_ok' => null]);

        return view('end', ['data' => $data]);
    }

    public function ko() {
        return view('ko');
    }
}
