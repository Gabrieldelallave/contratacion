<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function checkDocument(Request $request)
    {
        $ret = [];
        $file = $request->file('file');

        if ($file->isValid())
        {
            if (!@getimagesize($file))
            {
                $ret = ['error' => 'Formato de archivo incorrecto (sólo GIF, JPG o PNG).'];
            } else {
                $ext = toAscii($file->getClientOriginalExtension());
                if ('jpeg' == $ext) $ext = 'jpg';
                switch ($ext)
                {
                    case 'jpg': $src = imagecreatefromjpeg($file); break;
                    case 'gif': $src = imagecreatefromgif($file); break;
                    case 'png': $src = imagecreatefrompng($file); break;
                }

                //redim
                $wto = $hto = 1200;
                list($w, $h) = getimagesize($file);
                $r = $w / $h; //aspect
                if ($wto / $hto > $r)
                {
                    $wnew = $hto * $r;
                    $hnew = $hto;
                } else {
                    $hnew = $wto / $r;
                    $wnew = $wto;
                }

                $dst = imagecreatetruecolor($wnew, $hnew);
                imagefill($dst, 0, 0, imagecolorallocate($dst, 255, 255, 255));
                imagecopyresampled($dst, $src, 0, 0, 0, 0, $wnew, $hnew, $w, $h);

                $data = session('register_data');
                $dstFile = 'tmp-'.md5($data['idnumber']).'-'.time().'.jpg';
                imagejpeg($dst, sys_get_temp_dir().'/'.$dstFile, 90);

                $ret = ['url' => \URL::to('getFile').'?f=tmp/'.$dstFile.'&t='.time(), 'tmp' => $dstFile];
            }
        }
        echo json_encode($ret);
    }

    public function getFile(Request $request)
    {
        $file = $request['f'];

        // getFile sólo se usará para obtener ficheros de la carpeta temporal o de storage
        if ('tmp' == substr($file,0,3)) $file = sys_get_temp_dir().substr($file,3);
        else abort(500);

        // Evitar accesos a carpetas no deseados
        if (false !== strstr($file, '..') || false !== strstr($file, '//')) abort(500);

        if (isset($file) && file_exists($file) && is_file($file))
        {
            $mime = 'application/octet-stream';
            $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
            switch ($ext)
            {
                case 'jpg': case 'jpeg': $mime = 'image/jpeg'; break;
                case 'gif': $mime = 'image/gif'; break;
                case 'png': $mime = 'image/png'; break;
                case 'pdf': $mime = 'application/pdf'; break;
                // ...
            }

            header("Content-Type: $mime");
            readfile($file);
            exit;
        }

        abort(404);
    }
}
