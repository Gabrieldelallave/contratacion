<?php
//die();
/**
 * Cliente de pago de la plataforma Momopocket.
 *
 * Esta clase encapsula las llamadas necesarias a la API REST de Momopocket para comercios, necesarias para realizar las
 * operaciones de pago, devolución y anulacion, con dos modalidades de intercambio de activador:
 *
 * - Introducción directa de activador.
 * - Intercambio mediante código QR generado por el cliente.
 *
 * Esta clase necesita el paquete firebase/php-jwt (https://github.com/firebase/php-jwt)
 *
 * @author  Momopocket EDE SL
 * @version  1.1
 *
 */

/**
 * En el siguente include se incluye el cargador de composer para las dependencias, quitar si no se usa composer e in-
 * cluir las dependencias a mano.
 */
//include_once("php-jwt/Authentication/JWT.php");

namespace App;

use \Firebase\JWT\JWT;
use Exception;


class MomoAPICli {

    /*
     * Constantes de los dos entornos.momopo
     */
    const URL_BASE_OAUTH_PRE = 'https://oauth2.zonadeprueba.es';
    const URL_BASE_API_PRE   = 'https://api.zonadeprueba.es';

    const URL_BASE_OAUTH_PRO = 'https://oauth2.momopocket.com';
    const URL_BASE_API_PRO   = 'https://api.momopocket.com';

    protected $tokenAcceso;
    protected $tokenExp;
    protected $iss;
    protected $sub;
    protected $codComercio;
    protected $codTpv;
    protected $produccion = false;

    /**
     * Devuelve un array con distintas variables calculadas según el entorno en el que estamos.
     *
     * @return array
     */
    protected function getEntorno() {
        if ($this->produccion) return array(
            'URL_TOKEN' => self::URL_BASE_OAUTH_PRO . '/token',
            'URL_PAGO' => self::URL_BASE_API_PRO . '/v1.0/pagos',
            'URL_DEVOLUCION' => self::URL_BASE_API_PRO . '/v1.0/devoluciones',
            'URL_ANULACION' => self::URL_BASE_API_PRO . '/v1.0/anulaciones',
            'URL_EMPAREJAMIENTO' => self::URL_BASE_API_PRO . '/v1.0/emparejamientos/iniciar',
            'URL_VER_EMPAREJAMIENTO' => self::URL_BASE_API_PRO . '/v1.0/emparejamientos/comprobar',
            'PAT_URL_INI_OPER' => self::URL_BASE_API_PRO . '/v1.0/activadores/%s/inicio-operacion',
            'URL_INI_PAGO_WEB' => self::URL_BASE_API_PRO . '/v1.0/pago-web/iniciar',
            'URL_CMP_PAGO_WEB' => self::URL_BASE_API_PRO . '/v1.0/pago-web/comprobar'
        );

        return array(
            'URL_TOKEN' => self::URL_BASE_OAUTH_PRE . '/token',
            'URL_PAGO' => self::URL_BASE_API_PRE . '/v1.0/pagos',
            'URL_DEVOLUCION' => self::URL_BASE_API_PRE . '/v1.0/devoluciones',
            'URL_ANULACION' => self::URL_BASE_API_PRE . '/v1.0/anulaciones',
            'URL_EMPAREJAMIENTO' => self::URL_BASE_API_PRE . '/v1.0/emparejamientos/iniciar',
            'URL_VER_EMPAREJAMIENTO' => self::URL_BASE_API_PRE . '/v1.0/emparejamientos/comprobar',
            'PAT_URL_INI_OPER' => self::URL_BASE_API_PRE . '/v1.0/activadores/%s/inicio-operacion',
            'URL_INI_PAGO_WEB' => self::URL_BASE_API_PRE . '/v1.0/pago-web/iniciar',
            'URL_CMP_PAGO_WEB' => self::URL_BASE_API_PRE . '/v1.0/pago-web/comprobar'
        );
    }

    /**
     * Firma la url de la peticion REST con la clave privada.
     *
     * @param $metodo Método de la peticion GET, POST, ...
     * @param $url URL incluido el query string.
     * @return bool|string Devuelve la URL incluyendo la firma o false si hay error.
     */
    private function firmaUrl($metodo, $url) {

        $url .= (strpos($url,"?") ? "&" : "?");
        $url .= "ts=" . round(microtime(true) * 1000);

        $peticion = "$metodo $url";

        if (!openssl_sign($peticion,$digest,$this->clavePrivada, 'SHA256')) {
            return false;
        }

        $digest = urlencode(base64_encode($digest));
        return "$url&digest=$digest";

    }

    /**
     * Realiza una petición POST a la API.
     *
     * @param $url
     * @param $cuerpo
     * @param array $cabeceras
     * @return mixed Array con el resultado de la peticion.
     * @throws Exception
     */
    protected function peticionHttpPOST($url, $cuerpo, $cabeceras = array()) {
        $c = curl_init($url);

        curl_setopt($c, CURLOPT_POST, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, $cuerpo);
        if ($cabeceras) curl_setopt($c, CURLOPT_HTTPHEADER, $cabeceras);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        $resultado = curl_exec($c);

        // Analizamos el resultado.
        if(curl_errno($c)) {
            throw new Exception("Error en peticion POST al recurso $url: " . curl_error($c));
        }

        curl_close($c);

        return json_decode($resultado, true);
    }

    protected function peticionHttpGET($url, $cabeceras = array()) {
        $c = curl_init($url);

        curl_setopt($c, CURLOPT_HTTPGET, true);
        if ($cabeceras) curl_setopt($c, CURLOPT_HTTPHEADER, $cabeceras);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        $resultado = curl_exec($c);

        // Analizamos el resultado.
        if(curl_errno($c)) {
            throw new Exception("Error en peticion GET al recurso $url: " . curl_error($c));
        }

        curl_close($c);

        return json_decode($resultado, true);
    }

    /**
     * Realiza la petición del token de acceso al servidor de autorización.
     *
     * @throws Exception
     */
    protected function peticionToken() {
        $ent = $this->getEntorno();

        $ahora = time();
        $uid = ''.round(microtime(true) * 1000);

        $token = array(
            "iss" => $this->iss,
            "sub" => $this->sub,
            "aud" => array($ent['URL_TOKEN']),
            "jit" => "jit-$uid",
            "iat" => $ahora,
            "exp" => $ahora + 3600
        );

        $jwt = JWT::encode($token, $this->clavePrivada, "RS256");

        // Datos necesarios para la petición de token.
        $parametros = 'grant_type=' . urlencode('urn:ietf:params:oauth:grant-type:jwt-bearer') . "&assertion=$jwt";


        $res = $this->peticionHttpPOST($ent['URL_TOKEN'], $parametros);

        if (isset($res['status'])) {
            throw new Exception("Error $res[codigo], estatus $res[status] solicitando token: $res[mensaje]");
        }

        $this->tokenAcceso = $res['access_token'];
        $this->tokenExp = $ahora + $res['expires_in'];
    }

    /**
     * Inicia una operación de pago o devolución.
     *
     * @param $importe          Importe de la operación.
     * @param $activador        Activador del cliente.
     * @param $fecLocal         Feha local de la operacion.
     * @param $codOperExterno   Código de operación externo a la API.
     * @return mixed            Devuelve el código de activador.
     * @throws Exception
     */
    protected function peticionInicioOperacion($importe, $activador, $fecLocal, $codOperExterno) {
        // Verificamos token.
        if ($this->tokenExp <= time()) {
            $this->peticionToken();
        }

        $ent = $this->getEntorno();

        // URL de petición
        $url = sprintf($ent['PAT_URL_INI_OPER'],$activador);

        $parametros = array(
            "codComercio"         => $this->codComercio,
            "codTpv"              => $this->codTpv,
            "fecLocal"            => $fecLocal,
            "codOperacionExterno" => $codOperExterno,
            "divisa"              => "EUR",
            "exponente"           => 0,
            "importeBruto"        => $importe,
            "importeNeto"         => $importe
        );
        $cuerpoPet = json_encode($parametros);


        $cabecerasHttp = array(
            "Authorization: Bearer " . $this->tokenAcceso,
            "Accept-Charset: utf-8",
            "Content-type: application/json",
            "Content-length: " . strlen($cuerpoPet)
        );

        // Firmamos la petición.
        $url = $this->firmaUrl("POST",$url);

        // Ejecutamos la petición.
        $res = $this->peticionHttpPOST($url, $cuerpoPet, $cabecerasHttp);

        if (isset($res['status'])) {
            throw new Exception("Error $res[codigo], estatus $res[status] en inicio de operación: $res[mensaje]");
        }

        return $res['_datos']['activador']['codActivador'];
    }

    /**
     * Solicita el emparejamiento del tpv para un importe.
     *
     * @param $importe
     * @return mixed        Código de emparejamiento para mostrar en un QR.
     * @throws Exception
     */
    protected function peticionEmparejamiento($importe) {
        // Verificamos token.
        if ($this->tokenExp <= time()) {
            $this->peticionToken();
        }

        $ent = $this->getEntorno();

        // URL de petición
        $url = $ent['URL_EMPAREJAMIENTO'];

        $parametros = array(
            "codComercio" => $this->codComercio,
            "codTpv"      => $this->codTpv,
            "fecLocal"    => date('c'),
            "divisa"      => "EUR",
            "exponente"   => 0,
            "importe"     => $importe,
        );
        $cuerpoPet = json_encode($parametros);


        $cabecerasHttp = array(
            "Authorization: Bearer " . $this->tokenAcceso,
            "Accept-Charset: utf-8",
            "Content-type: application/json",
            "Content-length: " . strlen($cuerpoPet)
        );

        // Firmamos la petición.
        $url = $this->firmaUrl("POST",$url);

        // Ejecutamos la petición.
        $res = $this->peticionHttpPOST($url, $cuerpoPet, $cabecerasHttp);

        if (isset($res['status'])) {
            throw new Exception("Error $res[codigo], estatus $res[status] en emparejamiento: $res[mensaje]");
        }

        return $res['_datos']['codigoTpvEmparejar'];
    }

    /**
     * Comprueba si se ha establecido el emparejamiento en el servidor
     * @param $codEmparejamiento    Código de emparejamiento generado anteriormente.
     * @return integer|boolean      Código de activador del cliente o false si no hay emparejamiento.
     * @throws Exception
     */
    protected function peticionComprobarEmparejamiento($codEmparejamiento) {
        // Verificamos token.
        if ($this->tokenExp <= time()) {
            $this->peticionToken();
        }

        $ent = $this->getEntorno();

        // URL de petición
        $url = $ent['URL_VER_EMPAREJAMIENTO'];

        $url .= "?codComercio=$this->codComercio" .
                "&codTpv=$this->codTpv" .
                "&codigoTpvEmparejar=$codEmparejamiento";

        $cabecerasHttp = array(
            "Authorization: Bearer " . $this->tokenAcceso,
            "Accept-Charset: utf-8",
            "Content-type: application/json"
        );

        // Firmamos la petición.
        $url = $this->firmaUrl("POST",$url);

        // Ejecutamos la petición.
        $res = $this->peticionHttpPOST($url, '', $cabecerasHttp);

        if (isset($res['status'])) {
            throw new Exception("Error $res[codigo], estatus $res[status] comprobando emparejamiento: $res[mensaje]");
        }

        return $res['_datos']['codigoActivador'];
    }

    /**
     * Realiza la petición http a la API correspondiente a un pago, debe ir después de un inicio de operación.
     *
     * @param $importe          Importe de la operación.
     * @param $activador        Activador del cliente.
     * @param $fecLocal         Fecha local de operación.
     * @param $codOperExterno   Código de operación externo.
     * @return mixed            Identificador de la compra.
     * @throws Exception
     */
    protected function peticionPago($importe, $activador, $fecLocal, $codOperExterno) {
        // Verificamos token.
        if ($this->tokenExp <= time()) {
            $this->peticionToken();
        }

        $ent = $this->getEntorno();

        // URL de petición
        $url = $ent['URL_PAGO'];

        $parametros = array(
            "codComercio"         => $this->codComercio,
            "codTpv"              => $this->codTpv,
            "fecLocal"            => $fecLocal,
            "codOperacionExterno" => $codOperExterno,
            "divisa"              => "EUR",
            "exponente"           => 0,
            "codActivador"        => $activador,
            "importeBruto"        => $importe,
            "importeNetoOriginal" => $importe,
            "importeNetoFinal"    => $importe,
            "tsuIncluidoPrecio"   => false,
            "tipoCobro"           => "TOT"
        );
        $cuerpoPet = json_encode($parametros);

        $cabecerasHttp = array(
            "Authorization: Bearer " . $this->tokenAcceso,
            "Accept-Charset: utf-8",
            "Content-type: application/json",
            "Content-length: " . strlen($cuerpoPet)
        );

        // Firmamos la petición.
        $url = $this->firmaUrl("POST",$url);

        // Ejecutamos la petición.
        $res = $this->peticionHttpPOST($url, $cuerpoPet, $cabecerasHttp);

        if (isset($res['status'])) {
            throw new Exception("Error $res[codigo], estatus $res[status] en pago: $res[mensaje]");
        }

        return $res["_datos"]["idCompra"];
    }

    /**
     * Realiza la devolución de una compra anterior, es necesario el activador de la operación que se mostrará en la
     * app del cliente cuando elija devolver una compra.
     *
     * @param $importe          Importe de la operación.
     * @param $activador        Activador del cliente.
     * @param $fecLocal         Fecha local de operación.
     * @param $codOperExterno   Código de operación externo.
     * @return mixed            Identificador de la devolución.
     * @throws Exception
     */
    protected function peticionDevolucion($importe, $activador, $fecLocal, $codOperExterno) {
        // Verificamos token.
        if ($this->tokenExp <= time()) {
            $this->peticionToken();
        }

        $ent = $this->getEntorno();

        // URL de petición
        $url = $ent['URL_DEVOLUCION'];

        $parametros = array(
            "codComercio"         => $this->codComercio,
            "codTpv"              => $this->codTpv,
            "fecLocal"            => $fecLocal,
            "codOperacionExterno" => $codOperExterno,
            "divisa"              => "EUR",
            "exponente"           => 0,
            "codActivador"        => $activador,
            "importe"             => $importe,
            "tipoDevolucion"      => "SAL"
        );
        $cuerpoPet = json_encode($parametros);

        $cabecerasHttp = array(
            "Authorization: Bearer " . $this->tokenAcceso,
            "Accept-Charset: utf-8",
            "Content-type: application/json",
            "Content-length: " . strlen($cuerpoPet)
        );

        // Firmamos la petición.
        $url = $this->firmaUrl("POST",$url);

        // Ejecutamos la petición.
        $res = $this->peticionHttpPOST($url, $cuerpoPet, $cabecerasHttp);

        if (isset($res['status'])) {
            throw new Exception("Error $res[codigo], estatus $res[status] en devolución: $res[mensaje]");
        }

        return $res["_datos"]["idDevolucion"];
    }

    /**
     * Realiza la operación identificada por la pareja fechaLocalAAnular, $codOperacionAnular, en caso de que exista.
     *
     * @param $fecLocalAAnular
     * @param $codOperacionAAnular
     * @return mixed                Respuesta del servidor.
     * @throws Exception
     */
    protected function peticionAnulacion($fecLocalAAnular, $codOperacionAAnular) {
        // Verificamos token.
        if ($this->tokenExp <= time()) {
            $this->peticionToken();
        }

        $ent = $this->getEntorno();

        // URL de petición
        $url = $ent['URL_ANULACION'];

        $parametros = array(
            "codComercio"             => $this->codComercio,
            "codTpv"                  => $this->codTpv,
            "fecLocal"                => date('c'),
            "fecLocalOperacionAnular" => $fecLocalAAnular,
            "codOperacionExterno"     => $codOperacionAAnular
        );
        $cuerpoPet = json_encode($parametros);

        $cabecerasHttp = array(
            "Authorization: Bearer " . $this->tokenAcceso,
            "Accept-Charset: utf-8",
            "Content-type: application/json",
            "Content-length: " . strlen($cuerpoPet)
        );

        // Firmamos la petición.
        $url = $this->firmaUrl("POST",$url);

        // Ejecutamos la petición.
        $res = $this->peticionHttpPOST($url, $cuerpoPet, $cabecerasHttp);

        // No generamos escepción ya que puede ser normal que haya fallado.
        if (isset($res['status'])) {
            return $res;
        }

        return $res["_datos"];
    }

    protected function peticionInicioPagoWeb($importe, $fecLocal, $codOperExterno, $urlOK, $urlNOK, $nombreCompleto, $email, $articulos, $conceptos) {
        // Verificamos token.
        if ($this->tokenExp <= time()) {
            $this->peticionToken();
        }

        $ent = $this->getEntorno();

        // URL de petición
        $url = $ent['URL_INI_PAGO_WEB'];

        $parametros = array(
            "codComercio"         => $this->codComercio,
            "codTpv"              => $this->codTpv,
            "fecLocal"            => $fecLocal,
            "codOperacionExterno" => $codOperExterno,
            "divisa"              => "EUR",
            "exponente"           => 0,
            "importeTotal"        => $importe,
            "urlOK"               => $urlOK,
            "urlNOK"              => $urlNOK,
            "nombreCliente"       => $nombreCompleto,
            "emailCliente"        => $email
        );

        if (isset($articulos)) $parametros['articulos'] = $articulos;
        if (isset($conceptos)) $parametros['conceptos'] = $conceptos;
        $cuerpoPet = json_encode($parametros);


        $cabecerasHttp = array(
            "Authorization: Bearer " . $this->tokenAcceso,
            "Accept-Charset: utf-8",
            "Content-type: application/json",
            "Content-length: " . strlen($cuerpoPet)
        );

        // Firmamos la petición.
        $url = $this->firmaUrl("POST",$url);

        // Ejecutamos la petición.
        $res = $this->peticionHttpPOST($url, $cuerpoPet, $cabecerasHttp);

        if (isset($res['status'])) {
            abort($res['status'], "Error $res[codigo], estatus $res[status] en inicio de pago web: $res[mensaje]");
        }

        return $res['_datos']['urlRedireccion'];
    }


    protected function peticionComprobarPagoWeb($fecha, $uuid) {
        // Verificamos token.
        if ($this->tokenExp <= time()) {            
            $this->peticionToken();
        }

        $ent = $this->getEntorno();
        
        // URL de petición
        $url = $ent['URL_CMP_PAGO_WEB'] . "?uuid=$uuid&fecLocal=$fecha&codTpv=$this->codTpv&codComercio=$this->codComercio";

        $cabecerasHttp = array(
            "Authorization: Bearer " . $this->tokenAcceso,
            "Accept-Charset: utf-8",
            "Content-type: application/json",
        );
        
        // Firmamos la petición.
        $url = $this->firmaUrl("GET",$url);

        // Ejecutamos la petición.
        $res = $this->peticionHttpGET($url, $cabecerasHttp);

        if (isset($res['status'])) {
            throw new Exception("Error $res[codigo], estatus $res[status] en comprobación de pago web: $res[mensaje]");
        }
        
        
        return $res['_datos'];
    }

    /**
     * Constructor de la clase.
     *
     * @param $iss                  Issuer.
     * @param $sub                  Subject.
     * @param $clavePriv            Cadena conteniendo el la clave privada en formato x509 codificado en base64.
     * @param $codComercio          Código del comercio.
     * @param $codTpv               Código del TPV.
     * @param bool $produccion      Falso indica pruebas, true indica producción.
     * @throws Exception
     */
    public function __construct($iss, $sub, $clavePriv, $codComercio, $codTpv, $produccion = false) {
        $this->iss = $iss;
        $this->sub = $sub;
        $this->clavePrivada = $clavePriv;
        $this->codComercio = $codComercio;
        $this->codTpv = $codTpv;
        $this->produccion = $produccion;

        $this->peticionToken();
    }

    /**
     * Realiza una peticion de inicio de emparejamiento.
     *
     * @param $importe      Importe de la operación que se quiere realizar.
     * @return mixed        Devuelve el código de emparejamiento a mostrar via QR.
     * @throws Exception
     */
    public function emparejamiento($importe) {
        if (!isset($importe)) throw new Exception("Debe pasar un importe válido.");

        return $this->peticionEmparejamiento($importe);;
    }

    /**
     * Vefifica si se ha realizado el emparejamiento con el cliente y en caso afirmativo devuelve el activador.
     *
     * @param $codEmparejamiento
     * @return string       Activador del cliente para realizar el pago.
     * @throws Exception
     */
    public function hayEmparejamiento($codEmparejamiento) {
        if (!isset($codEmparejamiento)) throw new Exception("Debe pasar un código de emparejamiento válido.");

        $activador = '';
        try {
            $activador =  $this->peticionComprobarEmparejamiento($codEmparejamiento);
        }catch (Exception $e) {
            if (!preg_match('/No se ha encontrado el activador para el inicio del cobro/', $e->getMessage())) {
                throw $e;
            }
        }
        return $activador;
    }

    /**
     * Realiza las peticiones necesarias para ejecutar un pago con los parámetros especificados.
     *
     * @param $importe
     * @param $activador
     * @param $fecha
     * @param $codOperacion
     * @return array            Un array con los parámetros del pago.
     * @throws Exception
     */
    public function pago($importe, $activador, $fecha, $codOperacion) {

        if (!isset($importe)) throw new Exception("Debe pasar un importe válido.");
        if (!isset($activador)) throw new Exception("Debe pasar un activador.");
        if (!isset($fecha)) throw new Exception("Debe pasar una fecha de operación.");
        if (!isset($codOperacion)) throw new Exception("Debe pasar un codigo de operación propio.");

        $codActivador = $this->peticionInicioOperacion($importe, $activador, $fecha, $codOperacion);
        $idPago = $this->peticionPago($importe,$codActivador,$fecha,$codOperacion);

        return array(
            'idPago' => $idPago,
            'codActivador' => $codActivador,
            'importe' => $importe,
            'fecha' => $fecha,
            'codOperacion' => $codOperacion
        );

    }

    /**
     * Realiza las peticiones necesarias para ejecutar una devolución con los parámetros especificados.
     *
     * @param $importe
     * @param $activador
     * @param $fecha
     * @param $codOperacion
     * @return array            Un array con los datos de la devolución.
     * @throws Exception
     */
    public function devolucion($importe, $activador, $fecha, $codOperacion) {

        if (!isset($importe)) throw new Exception("Debe pasar un importe válido.");
        if (!isset($activador)) throw new Exception("Debe pasar un activador.");
        if (!isset($fecha)) throw new Exception("Debe pasar una fecha de operación.");
        if (!isset($codOperacion)) throw new Exception("Debe pasar un codigo de operación propio.");

        $codActivador = $this->peticionInicioOperacion($importe, $activador, $fecha, $codOperacion);
        $idDevolucion = $this->peticionDevolucion($importe,$codActivador,$fecha,$codOperacion);

        return array(
            'idDevolucion' => $idDevolucion,
            'codActivador' => $codActivador,
            'importe' => $importe,
            'fecha' => $fecha,
            'codOperacion' => $codOperacion
        );

    }

    /**
     * Realiza las peticiones necesarias para ejecutar una anulación con los parámetros especificados.
     *
     * @param $codOperacion
     * @return array            Un array con el resultado de la anulación.
     * @throws Exception
     */
    public function anulacion($fecha, $codOperacion) {
        if (!isset($fecha)) throw new Exception("Debe pasar una fecha válida.");
        if (!isset($codOperacion)) throw new Exception("Debe pasar un codigo de operación válido.");

        $anulacion = $this->peticionAnulacion($fecha, $codOperacion);

        return $anulacion;
    }

    /**
     * Realiza la petición de inicio d pago web para obtener una url de redirección donde mandar al cliente.
     *
     * @param $importe          Importe del pago.
     * @param $fecha            Fecha/Hora del pago en formato ISO8601
     * @param $codOperacion     Codigo de operación propio que identificará la operación junto con la fecha.
     * @param $urlOK            Url de vuelta si la operación va bien.
     * @param $urlNOK           Url de vuelta si la operacion tiene errores.
     * @param $nombreCompleto   Nombre completo de cliente.
     * @param $email            Dirección de correo de cliente.
     * @param $articulos        (opcional) Lista de articulos de la compra.
     * @param $conceptos        (opcional) Lista de conceptos a mostrar.
     * @return String           Url a la que redirigir el navegador.
     * @throws Exception
     */
    public function inicioPagoWeb($importe, $fecha, $codOperacion, $urlOK, $urlNOK, $nombreCompleto, $email, $articulos = null, $conceptos = null) {
        if (!isset($importe)) throw new Exception("Debe pasar un importe válido.");
        if (!isset($fecha)) throw new Exception("Debe pasar una fecha de operación.");
        if (!isset($codOperacion)) throw new Exception("Debe pasar un codigo de operación propio.");
        if (!isset($urlOK)) throw new Exception("Debe pasar una URL de retorno si la operación es correcta.");
        if (!isset($urlNOK)) throw new Exception("Debe pasar una URL de retorno si la operación es incorrecta.");
        if (!isset($nombreCompleto)) throw new Exception("Debe pasar el nombre completo del usuario.");
        if (!isset($email)) throw new Exception("Debe pasar el email del usuario.");

        $urlPagoWeb = $this->peticionInicioPagoWeb($importe, $fecha, $codOperacion, $urlOK, $urlNOK, $nombreCompleto, $email, $articulos, $conceptos);
        
        return $urlPagoWeb;
    }

    /**
     * Verifica que la peticion recibida corresponde a un pago web válido.
     *
     * @param $uuid         Identificador del pago web.
     * @return mixed        Objeto con datos del pago.
     * @throws Exception
     */
    public function comprobarPagoWeb($fecha, $uuid ) {
        if (!isset($fecha)) throw new Exception("Debe pasar la fecha de inicio del pago web.");
        if (!isset($uuid)) throw new Exception("Debe pasar el uuid del pago web.");
        $datosPagoWeb = $this->peticionComprobarPagoWeb($fecha, $uuid);

        return $datosPagoWeb;
    }
}
