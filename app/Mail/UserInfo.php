<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Http\Request;

class UserInfo extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $data = session('register_data');
        $this->to($data['email'])
             ->from(config('contratacion.info-email3'))
             ->subject(t('app.user-info.subject'));
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->markdown('emails.user-info')
            ->attach(asset('storage/documentos/FichaAlta.docx'), [
                'as' => 'Ficha Alta.docx',
                'mime' => 'application/docx',
            ])
            ->attach(asset('storage/documentos/MandatoDomiciliacion.pdf'), [
                'as' => 'Mandato Domiciliacion.pdf',
                'mime' => 'application/pdf',
            ])
            ->with(['data' => $this->data]);
    }
}
