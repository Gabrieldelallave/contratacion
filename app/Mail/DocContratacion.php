<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Http\Request;

class DocContratacion extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $data = session('register_data');
        $this->to(config('contratacion.info-email1'))
             ->from($data['email'])
             ->subject(t('app.doc-contratacion.subject') .': '. $data['phone']);
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = session('register_data');

        $ext = explode('.', $data['iddoc1file']);
        $ext = strtolower($ext[count($ext)-1]);

        $baseIdDocName = 'contratacion-' . $data['idnumber'] .'-'. time();

        $obj = $this
            ->markdown('emails.doc-contratacion')
            ->attach(sys_get_temp_dir().'/'.$data['iddoc1file'], ['as' => $baseIdDocName . '-1.' . $ext]);

        if (isset($data['iddoc2file']) && $data['iddoc1file'])
        {
            $obj->attach(sys_get_temp_dir().'/'.$data['iddoc2file'], ['as' => $baseIdDocName . '-2.' . $ext]);
        }

        if (isset($data['iddoc3file']) && $data['iddoc3file'])
        {
            $obj->attach(sys_get_temp_dir().'/'.$data['iddoc3file'], ['as' => $baseIdDocName . '-pasaporte.' . $ext]);
        }

        return $obj->with(['data' => $this->data]);
    }
}
